public class Buku {
    String title;
    String author;
    String ISBN;
    double price;
    int quantity;

    /*
     * Overloading constructor adalah dua constructor berbeda dengan nama yang sama.
     * Constructor buku yang pertama digunakan jika buku tersebut hanya memeiliki beberapa parameter saja.
     * Constructor buku pertama memiliki nilai default, sehingga jika terdapat parameter yang kosong maka akan diisi dengan nilai default.
     * Constructor buku kedua digunakan ketika data dalam buku tersebut telah lengkap.
     */
    
    public Buku() {
        this.title = "Unknown";
        this.author = "Unknown";
        this.ISBN = "Unknown";
        this.price = 0.0;
        this.quantity = 0;
    }

    public Buku(String title, String author, String ISBN, double price, int quantity) {
    this.title = title;
    this.author = author;
    this.ISBN = ISBN;
    this.price = price;
    this.quantity = quantity;
    }

    /* 
     * Overloading method yaitu method dengan nama yang sama akan tetapi memiliki parameter berbeda.
     * Method updateQuantity adalah 2 method yang berbeda.
     * updateQuantity pertama digunakan untuk memberikan jumlah buku.
     * updateQuantity kedua digunakan untuk mengubah jumlah buku yang sudah ada dengan operasi kurang atau tambah.
     */
    
    public void updateQuantity(int newQuantity) {
        this.quantity = newQuantity;
    }
    
    public void updateQuantity(String operation, int quantity) {
        if (operation.equals("+")) {
            this.quantity += quantity;
        } else if (operation.equals("-")) {
            this.quantity -= quantity;
        }
    }    
}