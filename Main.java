public class Main {
    public static void main(String[] args) {
        Buku book1 = new Buku();
        System.out.println(book1.title); // Unknown
        System.out.println(book1.author); // Unknown
        System.out.println(book1.price); // 0.0
    
        Buku book2 = new Buku("Introduction to Algorithms", "Thomas H Cormen, Charles E Leiserson", "0262258102", 50000, 10);
        System.out.println(book2.title); // Introduction to Algorithms
        System.out.println(book2.price); // 50000

        System.out.println(book2.quantity); // 10
        
        book2.updateQuantity("+", 2);
        System.out.println(book2.quantity); // 12
        
        book2.updateQuantity(5);
        System.out.println(book2.quantity); // 5
        
        book2.updateQuantity("+", 3);
        System.out.println(book2.quantity); // 8
    }    
}